﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonneForm.Models
{
    public class Personne
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public int Age { get; set; }
        public string Description { get; set; }

        public Personne(int id, string nom, string prenom, int age, string description)
        {
            Id = id;
            Nom = nom;
            Prenom = prenom;
            Age = age;
            Description = description;
        }
        public Personne(int id, string nom, string prenom)
        {
            Id = id;
            Nom = nom;
            Prenom = prenom;
        }
        public Personne()
        {
        }
    }
}
