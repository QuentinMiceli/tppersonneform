﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonneForm.ViewModels
{
    class FormulaireViewModel : BaseViewModel
    {
        private int id;
        private string nom;
        private string prenom;
        private int age;
        private string description;

        public int Id
        {
            get { return id; }
            set
            {
                if (value != id)
                {
                    id = value;
                    OnPropertyChanged();
                }
            }
        }
        public string Nom
        {
            get { return nom; }
            set {
                if (value != nom)
                {
                    nom = value;
                    OnPropertyChanged();
                }
            }
        }
        public string Prenom
        {
            get { return prenom; }
            set
            {
                if (value != prenom)
                {
                    prenom = value;
                    OnPropertyChanged();
                }
            }
        }
        public int Age
        {
            get { return age; }
            set
            {
                if (value != age)
                {
                    age = value;
                    OnPropertyChanged();
                }
            }
        }
        public string Description
        {
            get { return description; }
            set
            {
                if (value != description)
                {
                    description = value;
                    OnPropertyChanged();
                }
            }
        }

        public FormulaireViewModel()
        {
        }

    }
}
