﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PersonneForm.Models;
using System.Text.RegularExpressions;
using PersonneForm.ViewModels;

namespace PersonneForm
{
    /// <summary>
    /// Logique d'interaction pour Formulaire.xaml
    /// </summary>
    public partial class Formulaire : Window
    {
        
        Personne personne = new Personne();
        List<Personne> personnes = new List<Personne>();

        public Formulaire()
        {
            InitializeComponent();
            DataContext = new FormulaireViewModel();
            dgPersonne.ItemsSource = personnes;
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbNom.Text) ||
                string.IsNullOrEmpty(tbPrenom.Text) ||
                string.IsNullOrEmpty(tbAge.Text))
            {
                MessageBox.Show("Des champs sont vides");
            }
            else
            {
                //Test View Model

                var vm = this.DataContext as FormulaireViewModel;

                vm.Id++;
                Personne newPersonne = new Personne(vm.Id, vm.Nom, vm.Prenom, vm.Age, vm.Description);

                personnes.Add(newPersonne);
                dgPersonne.Items.Refresh();

                vm.Nom = "";
                vm.Prenom = "";
                vm.Age = 0;
                vm.Description = "";
            }
        }

        private void Modifier_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbNom.Text) ||
                string.IsNullOrEmpty(tbPrenom.Text) ||
                string.IsNullOrEmpty(tbDescription.Text) ||
                string.IsNullOrEmpty(tbAge.Text))
            {
                MessageBox.Show("Des champs sont vides");
            }
            else
            {
                personne.Nom = tbNom.Text;
                personne.Prenom = tbPrenom.Text;
                personne.Age = int.Parse(tbAge.Text);
                personne.Description = tbDescription.Text;

                dgPersonne.Items.Refresh();

                ok.Visibility = Visibility.Visible;
                modifier.Visibility = Visibility.Collapsed;

                tbNom.Text = "";
                tbPrenom.Text = "";
                tbAge.Text = "";
                tbDescription.Text = "";
            }
        }
        private void Annuler_Click(object sender, RoutedEventArgs e)
        {
            ok.Visibility = Visibility.Visible;
            modifier.Visibility = Visibility.Collapsed;

            tbNom.Text = "";
            tbPrenom.Text = "";
            tbAge.Text = "";
            tbDescription.Text = "";
        }

        private void X_Click(object sender, RoutedEventArgs e)
        {
            Personne personne = dgPersonne.SelectedItem as Personne;
            if (personne != null)
            {
                personnes.Remove(personne);
                dgPersonne.Items.Refresh();
            }
        }

        private void M_Click(object sender, RoutedEventArgs e)
        {
            ok.Visibility = Visibility.Collapsed;
            modifier.Visibility = Visibility.Visible;

            personne = dgPersonne.SelectedItem as Personne;

            tbNom.Text = personne.Nom;
            tbPrenom.Text = personne.Prenom;
            tbAge.Text = personne.Age.ToString();
            tbDescription.Text = personne.Description;
        }

        private void NumberValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
